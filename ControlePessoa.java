 import java.util.ArrayList;

public class ControlePessoa {

//atributos

    private ArrayList<Pessoa> listaPessoas;

//construtor

    public ControlePessoa() {
        listaPessoas = new ArrayList<Pessoa>();
    }

// métodos
   
    public String adicionar(Pessoa umaPessoa) {
        String mensagem = "Pessoa adicionada com Sucesso!";
        listaPessoas.add(umaPessoa);
        return mensagem;
    }


    public remover(Pessoa umPessoa) {
    	  String mensagem = "Pessoa removida com Sucesso!";
        listaPessoas.remove(umPessoa);
        return mensagem;
    }
    
    public Pessoa pesquisar(String nome) {
        for (Pessoa pessoa: listaPessoas) {
            if (pessoa.getNome().equalsIgnoreCase(nome))
            	return pessoa;
        }
        return null;
    }

}
